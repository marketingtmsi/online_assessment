var authenticator = {
  checkSignIn: function (req, res, next) {
    if (req.session.user) {
      next();
    } else {
      var err = new Error('You are not authorised to view this page.');
      next(err);
    }
  }
}

module.exports = authenticator;