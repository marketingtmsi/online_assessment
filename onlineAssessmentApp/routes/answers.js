var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var auth = require('./authenticator');

var Answer = require('../models/answers');
var Question = require('../models/questions');
var Page = require('../models/pages')

/* GET answers listing */
router.get('/:qnid([A-Za-z0-9]+)', auth.checkSignIn, function(req, res, next) {
    Answer.find({username: req.session.user, qid: req.params.qnid, attempt: {$gt: 0}}, '-pages', function(err, answersResult) {
        Question.findOne({_id: req.params.qnid}, 'name', function(err, questionsResult) {
            if (err) throw err;
            res.render('answers', {username: req.session.user, name: questionsResult.name, answerslist: answersResult});
        });
    });
});

router.get('/:qnid([A-Za-z0-9]+)/attempts/:ano([0-9]+)/pages/:pno([0-9]+)', auth.checkSignIn, function(req, res, next) {
    var current_page = parseInt(req.params.pno);
    Answer.find({username: req.session.user, qid: req.params.qnid, attempt: req.params.ano}, {pages: {$slice: [current_page - 1,1]}}, function(err, answersResult) {
        var image_answer = answersResult[0].pages[0];
        Question.findOne({_id: req.params.qnid}, 'pages', function(err, questionsResult) {
            var total_pages = questionsResult.pages.length;
            var next_page = current_page + 1;
            var previous_page = current_page - 1;
            var next_path = '';
            var previous_path = '';

            if (current_page == 1) {
                next_path = '/answers/' + req.params.qnid + '/attempts/' + req.params.ano + '/pages/' + next_page;
            } else if (current_page < total_pages) {
                next_path = '/answers/' + req.params.qnid + '/attempts/' + req.params.ano + '/pages/' + next_page;
                previous_path = '/answers/' + req.params.qnid + '/attempts/' + req.params.ano + '/pages/' + previous_page;
            } else {
                previous_path = '/answers/' + req.params.qnid + '/attempts/' + req.params.ano + '/pages/' + previous_page;
            }
            Page.findById(questionsResult.pages[req.params.pno-1], 'image', function(err, pagesResult) {
                res.render('answer', {attempts_link: '/answers/' + req.params.qnid, username: req.session.user, error: false, page: req.params.pno, total_pages: total_pages, next_action: next_path, previous_action: previous_path, image_src_qn: pagesResult.image, image_src_ans: image_answer});
            });
        });
    });
});

module.exports = router;