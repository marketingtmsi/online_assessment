var express = require('express');
var router = express.Router();

var fs = require('fs');
var auth = require('./authenticator');
var answers_path = '/answers';

var Question = require('../models/questions');
var Page = require('../models/pages');
var Answer = require('../models/answers');

/* GET questions listing. */
router.get('/', auth.checkSignIn, function (req, res, next) {
  console.log('session id ' + req.sessionID);
  console.log('session maxAge ' + req.session.cookie.expires + ' ' + req.session.cookie.maxAge);
  Question.find({}, '-pages', function (err, questionsResult) {
    if (questionsResult.length > 0) {
      res.render('questions', { username: req.session.user, questionslist: questionsResult, answers_link: answers_path });
    } else {
      res.render('questions', { username: req.session.user });
    }
  });
});

router.get('/:sno([0-9]+)/pages/:pno([0-9]+)', auth.checkSignIn, function (req, res, next) {
  var set_num = req.params.sno;
  var page_num = req.params.pno;
  var message_str = '';
  if (page_num > 1) {
    message_str = 'Your answer to page ' + (page_num - 1) + ' has been saved.';
  }
  Question.findOne({ set: set_num }, function (err, questionsResult) {
    var page_id = questionsResult.pages[page_num - 1];
    Page.findById(page_id, function (err, pagesResult) {
      try {
        res.render('question', { username: req.session.user, message: message_str, page: page_num, total_pages: questionsResult.pages.length, img_src: pagesResult.image, form_action: '/questions/' + set_num + '/pages/' + page_num, qid: questionsResult._id });
      } catch (e) {
        res.render('question', { username: req.session.user, message: 'error', error: true });
      }
    });
  });
});

/* POST save answer to question */
router.post('/:sno([0-9]+)/pages/:pno([0-9]+)', function (req, res, next) {
  var image_data = req.body['canvas_answer'];
  var questions_id = req.body['qid'];
  image_data_trunc = image_data.substring(image_data.indexOf(',') + 1) //in base64
  console.log('questions_id ' + questions_id);
  console.log('no of bytes in answer ' + Buffer.byteLength(image_data, 'base64'));
  var buffer = Buffer.from(image_data_trunc, 'base64');
  fs.writeFileSync('answer_page_' + req.params.pno + '.png', buffer);
  if (req.params.pno == 1) {
    var answers_instance = new Answer({username: req.session.user, attempt: 0, qid: questions_id, pages: [image_data] });
    answers_instance.save(function (err) {
      if (err) throw err;
    });
  } else {
    Answer.updateOne({username: req.session.user, attempt: 0, qid: questions_id}, {$push: {pages: image_data}}, function(err, answersResult) {
      if (err) throw err;
    });
  }

  Question.findOne({ set: req.params.sno }, function (err, questionsResult) {
    var next = parseInt(req.params.pno) + 1;
    var total_pages = questionsResult.pages.length;
    if (next <= total_pages) {
      res.redirect('/questions/' + req.params.sno + '/pages/' + next);
    } else {
      var attempt_num = 0;
      Answer.find({username: req.session.user, qid: questions_id}, 'attempt', function(err, answersResult) {
        attempt_num = answersResult.length;
      })
      Answer.updateOne({username: req.session.user, attempt: 0, qid: questions_id}, {attempt: attempt_num}, function(err, answersResult) {
        if (err) throw err;
      });
      message_str = 'Your answers have been saved. Please provide at least 1 day for marking and grading.';
      Question.find({}, function (err, questionsResult) {
        if (questionsResult.length > 0) {
          res.render('questions', { message: message_str, username: req.session.user, questionslist: questionsResult });
        }
      });
    }
  });
});

module.exports = router;




